#! /bin/bash

set -e
SOURCE_DIR=~/ffmpeg_sources
BUILD_DIR=/ffmpeg_build
export LDFLAGS='-no-pie'
export ELDFLAGS='-no-pie'

# Check if ran with root permissions
if [ `id -u` -ne 0 ]; then
   printf "The script must be run as root!\n"
   exit 1
fi

apt-get update -q

apt-get install -yq \
    autoconf \
    automake \
    build-essential \
    libass-dev \
    libfreetype6-dev \
    libtheora-dev \
    libtool \
    libvorbis-dev \
    pkg-config \
    texinfo \
    wget \
    zlib1g-dev \
    cmake \
    mercurial \
    git

apt-get clean

rm -rf $SOURCE_DIR
mkdir $SOURCE_DIR
cd $SOURCE_DIR

wget http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz \
  && tar xzvf yasm-1.3.0.tar.gz \
  && cd yasm-1.3.0 \
  && ./configure --prefix="$BUILD_DIR" --bindir="/bin" \
  && make \
  && make install || exit 1

cd $SOURCE_DIR

wget http://www.nasm.us/pub/nasm/releasebuilds/2.13.02/nasm-2.13.02.tar.bz2 \
  && tar xjvf nasm-2.13.02.tar.bz2 \
  && cd nasm-2.13.02 \
  && ./autogen.sh \
  && PATH="/bin:$PATH" ./configure --prefix="$BUILD_DIR" --bindir="/bin" \
  && PATH="/bin:$PATH" make \
  && make install || exit 1

cd $SOURCE_DIR

wget http://download.videolan.org/pub/x264/snapshots/last_stable_x264.tar.bz2 \
  && tar xjvf last_stable_x264.tar.bz2 \
  && cd x264-snapshot* \
  && PATH="/bin:$PATH" ./configure --prefix="$BUILD_DIR" --bindir="/bin" --enable-static --disable-opencl \
  && PATH="/bin:$PATH" make \
  && make install || exit 1


##&& PATH="/bin:$PATH" ./configure --prefix="$BUILD_DIR" --bindir="/bin" --enable-pic --enable-shared --disable-opencl \
##&& PATH="/bin:$PATH" ./configure --prefix="$BUILD_DIR" --bindir="/bin" --enable-static --enable-pic --enable-shared --disable-opencl \

cd $SOURCE_DIR

hg clone -u stable https://bitbucket.org/multicoreware/x265 \
  && cd x265/build/linux \
  && PATH="/bin:$PATH" cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX="$BUILD_DIR" -DENABLE_SHARED:bool=off ../../source \
  && make \
  && make install || exit 1

cd $SOURCE_DIR

wget -O fdk-aac.tar.gz https://github.com/mstorsjo/fdk-aac/tarball/v0.1.5 \
  && tar xzvf fdk-aac.tar.gz \
  && cd mstorsjo-fdk-aac* \
  && autoreconf -fiv \
  && ./configure --prefix="$BUILD_DIR" --disable-shared \
  && make \
  && make install || exit 1

cd $SOURCE_DIR

wget http://downloads.sourceforge.net/project/lame/lame/3.100/lame-3.100.tar.gz \
  && tar xzvf lame-3.100.tar.gz \
  && cd lame-3.100 \
  && ./configure --prefix="$BUILD_DIR" --enable-nasm --disable-shared \
  && make \
  && make install || exit 1

cd $SOURCE_DIR

wget https://archive.mozilla.org/pub/opus/opus-1.2.1.tar.gz \
  && tar xzvf opus-1.2.1.tar.gz \
  && cd opus-1.2.1 \
  && ./configure --prefix="$BUILD_DIR" --disable-shared \
  && make \
  && make install || exit 1

cd $SOURCE_DIR

git clone https://chromium.googlesource.com/webm/libvpx.git \
  && cd libvpx \
  && git checkout e20ca4fead6e48c2af1a5cff05b97c4b4cf2526c \
  && PATH="/bin:$PATH" ./configure --prefix="$BUILD_DIR" --disable-examples --disable-unit-tests --enable-vp9-highbitdepth \
  && PATH="/bin:$PATH" make \
  && make install || exit 1

cd $SOURCE_DIR

wget http://ffmpeg.org/releases/ffmpeg-3.4.4.tar.bz2 \
  && tar xjvf ffmpeg-3.4.4.tar.bz2 \
  && cd ffmpeg-3.4.4 \
  && PATH="/bin:$PATH" PKG_CONFIG_PATH="$BUILD_DIR/lib/pkgconfig" ./configure \
    --prefix="$BUILD_DIR" \
    --pkg-config-flags="--static" \
    --extra-cflags="-I$BUILD_DIR/include" \
    --extra-ldflags="-L$BUILD_DIR/lib" \
    --bindir="/bin" \
    --enable-gpl \
    --enable-libass \
    --enable-libfdk-aac \
    --enable-libfreetype \
    --enable-libmp3lame \
    --enable-libopus \
    --enable-libtheora \
    --enable-libvorbis \
    --enable-libvpx \
    --enable-libx264 \
    --enable-libx265 \
    --enable-nonfree \
  && PATH="/bin:$PATH" make \
  && make install \
  && hash -r || exit 1

cd / && rm -r $SOURCE_DIR
